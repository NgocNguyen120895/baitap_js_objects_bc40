
var staffArr = [];
var staffList = "STAFFLIST"


var JSONSTAFFLIST = localStorage.getItem(staffList);
if (JSONSTAFFLIST != null) {
    var listArr = JSON.parse(JSONSTAFFLIST);
    staffArr = listArr.map(function (item) {
        return new NhanVien(item.taikhoan, item.hoten, item.email, item.matkhau,
            item.ngaylam,
            item.luongCB,
            item.chucvuvalue,
            item.chucvutxt,
            item.tonggiolam);
    });
    renderStaff(staffArr);
}

function saveToJSON(data) {
    var staffListJSON = JSON.stringify(data);
    localStorage.setItem("STAFFLIST", staffListJSON);
    return staffListJSON;
}

function addStaff() {
    /**
     * 1. Grab staff detail from layThongTinTuForm()
     * 2. Push staff details to staffArr array
     * 3. Save to local storage saveToJSON(arr)
     * 4. Render staff list to the table renderStaff()
     */

    var nhanVien = layThongTinTuForm();

    var isAllowed = true;
    var validID, validName, validEmail, validPass, validSalary, validTime, validPosition;
    validID = kiemTraTrung(nhanVien.taikhoan, staffArr) && kiemTraDoDai(4, 8, nhanVien.taikhoan, "tbTKNV");
    validName = kiemTraText(nhanVien.hoten, "tbTen");
    validEmail = kiemTraEmail(nhanVien.email);
    validDate = kiemTraNgay(nhanVien.ngaylam);
    validPass = kiemTraMatKhau(nhanVien.matkhau);
    validSalary = kiemTraLuong(nhanVien.luongCB);
    validTime = kiemTraGioLam(80, 200, nhanVien.tonggiolam);  
    validPosition = kiemTraChucVu(nhanVien.chucvuvalue);
    
    console.log(validTime);

    isAllowed = validID & validName & validEmail & validDate & validPass & validSalary & validTime & validPosition;

    if (isAllowed) {
        staffArr.push(nhanVien);
        saveToJSON(staffArr);
        renderStaff(staffArr);
        clearValue();
    }
}

function onDeleteStaff(tknv) {
    var viTriDel = timViTri(tknv, staffArr);

    if (viTriDel != -1) {
        staffArr.splice(viTriDel, 1);
        renderStaff(staffArr);
    }
    saveToJSON(staffArr);
}
var TAIKHOAN = document.getElementById("tknv");


function onEdit(tknv) {

    document.getElementById('btnThemNV').disabled = true;

    clearNotices();
    var viTriEdit = timViTri(tknv, staffArr);

    if (viTriEdit == -1) {
        return;
    }
    var nv = staffArr[viTriEdit];


    TAIKHOAN.value = nv.taikhoan;
    TAIKHOAN.disabled = true;
    document.getElementById("name").value = nv.hoten;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.matkhau;
    document.getElementById("datepicker").value = nv.ngaylam;
    document.getElementById("luongCB").value = nv.luongCB;
    document.getElementById("chucvu").text = nv.chucvutxt;
    document.getElementById("gioLam").value = nv.tonggiolam;


}

function onUpdate() {
    var nhanVien = layThongTinTuForm();

    var viTri = timViTri(nhanVien.taikhoan, staffArr);

    if (viTri != -1) {
        var validID, validName, validEmail, validPass, validDate, validSalary, validTime;
        validID = kiemTraTrung(nhanVien.taikhoan, staffArr) && kiemTraDoDai(4, 8, nhanVien.taikhoan, "tbTKNV");
        validName = kiemTraText(nhanVien.hoten, "tbTen");
        validEmail = kiemTraEmail(nhanVien.email);
        validDate = kiemTraNgay(nhanVien.ngaylam);
        validPass = kiemTraMatKhau(nhanVien.matkhau);
        validSalary = kiemTraLuong(nhanVien.luongCB);
        validTime = kiemTraGioLam(80, 200, nhanVien.tonggiolam);

        isAllowed = validName & validEmail & validDate & validPass & validSalary & validTime;
        if (isAllowed) {
            staffArr[viTri] = nhanVien;
            renderStaff(staffArr);
        }
    }
    clearValue();
    saveToJSON(staffArr);
    clearNotices();
}

var clearValue = () => {
    TAIKHOAN.disabled = false;
    document.getElementById('btnThemNV').disabled = false;
    document.getElementById("tknv").value = "";
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("datepicker").value = "";
    document.getElementById("luongCB").text = "";
    document.getElementById("chucvu").value = "";
    document.getElementById("gioLam").value = "";
}

var clearNotices = () => {
    document.getElementById('tbTKNV').style.display = "none";
    document.getElementById('tbTen').style.display = "none";
    document.getElementById('tbEmail').style.display = "none";
    document.getElementById('tbMatKhau').style.display = "none";
    document.getElementById('tbNgay').style.display = "none";
    document.getElementById('tbLuongCB').style.display = "none";
    document.getElementById('tbChucVu').style.display = "none";
    document.getElementById('tbGiolam').style.display = "none";

}