
function NhanVien(TAIKHOAN, HOTEN, EMAIL, MATKHAU, NGAYLAM, LUONGCB, CHUCVUVALUE, CHUCVUTXT, TONGGIOLAM) {
    this.taikhoan = TAIKHOAN;
    this.hoten = HOTEN;
    this.email = EMAIL;
    this.matkhau = MATKHAU;
    this.ngaylam = NGAYLAM;
    this.luongCB = LUONGCB;
    this.chucvuvalue = CHUCVUVALUE;
    this.chucvutxt = CHUCVUTXT;
    this.tonggiolam = TONGGIOLAM;
    this.tongLuong = function (value) {
        return this.luongCB * value;
    }

    this.xepLoai = (value) => {
        if (value >= 192) {
            return `Xuất sắc`;
        } else if (value >= 176) {
            return `Giỏi`;
        } else if (value >= 160) {
            return `Khá`;
        } else {
            return `Trung bình`;
        }
    }
}