

function layThongTinTuForm() {

    var taiKhoan = document.getElementById("tknv").value;
    var hoTen = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var matkhau = document.getElementById("password").value;
    var ngayLam = document.getElementById("datepicker").value;
    var luongCB = document.getElementById("luongCB").value;
    var chucVu = document.getElementById("chucvu");
    var chucVu_Value = chucVu.value;
    var chucVuText = chucVu.options[chucVu.selectedIndex].text;
    var tongGioLam = document.getElementById("gioLam").value;
    return new NhanVien(taiKhoan, hoTen, email, matkhau, ngayLam, luongCB, chucVu_Value,
        chucVuText, tongGioLam);
}

function renderStaff(arr) {
    var contentTBHTML = "";
    for (var i = 0; i < arr.length; i++) {
        var item = arr[i];
        var content = `<tr>
        <td>${item.taikhoan}</td>
        <td>${item.hoten}</td>
        <td>${item.email}</td>
        <td>${item.ngaylam}</td>
        <td>${item.chucvutxt}</td>
        <td>${item.tongLuong(item.chucvuvalue)}</td>
        <td>${item.xepLoai(item.tonggiolam)}</td> 
        <td>
        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" 
        id="btnmodal" onclick="onEdit('${item.taikhoan}')">Edit</button>
        <button class="btn btn-danger" onclick="onDeleteStaff('${item.taikhoan}')">Xóa</button></td>  
        </tr>`;
        contentTBHTML += content;
    }
    document.getElementById("table").innerHTML = contentTBHTML;
}

function timViTri(tk, arr) {
    var viTriTim = -1;
    for (var i = 0; i < arr.length; i++) {
        var nhanVien = arr[i];
        if (nhanVien.taikhoan == tk) {
            viTriTim = i;
            break;
        }
    }
    return viTriTim;
}

// VALIDATION 
function kiemTraTrung(tk, arr) {
    var index = arr.findIndex(function (check) {
        return tk == check.taikhoan;
    });
    if (index == -1) {
        document.getElementById("tbTKNV").style.display = "none";
        document.getElementById("tbTKNV").innerText = "";
        return true;
    } else {
        document.getElementById("tbTKNV").style.display = "block";
        document.getElementById("tbTKNV").innerText = `Tài khoản đã tồn tại, vui lòng nhập lại!`;
        return false;
    }
}

function kiemTraDoDai(min, max, valueCheck, errorOutput) {
    if (valueCheck.length < min || valueCheck.length > max) {
        document.getElementById(errorOutput).style.display = "block";
        document.getElementById(errorOutput).innerText = `Độ dài phải từ ${min} đến ${max} ký tự.`
        return false;
    } else {
        document.getElementById(errorOutput).style.display = "none";
        return true;
    }
}

function kiemTraText(value, errorOutput) {
    var reg = /^[A-Za-z][-a-zA-Z ]+$/;
    var isText = reg.test(value);
    if (value == "") {
        document.getElementById(errorOutput).style.display = "block";
        document.getElementById(errorOutput).innerHTML = "Vui lòng điền vào trường này";
        return false;
    } else if (!isText) {
        document.getElementById(errorOutput).style.display = "block";
        document.getElementById(errorOutput).innerHTML = `Tên phải là chữ`;
        return false;
    } else {
        document.getElementById(errorOutput).style.display = "none";
        return true;
    }
}

function kiemTraEmail(value) {
    var reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = reg.test(value);
    if (isEmail) {
        document.getElementById("tbEmail").style.display = "none";
        return true;
    } else if (value == "") {
        document.getElementById("tbEmail").style.display = "block";
        document.getElementById("tbEmail").innerHTML = `Vui lòng điền email`;
        return false;
    } else {
        document.getElementById("tbEmail").style.display = "block";
        document.getElementById("tbEmail").innerHTML = `Email không hợp lệ`;
        return false;
    }
}

function kiemTraNgay(value) {
    var d_reg = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/
    if (d_reg.test(value)) {
        document.getElementById("tbNgay").innerHTML = "";
        return true;
    } else if (value == "") {
        document.getElementById("tbNgay").style.display = "block";
        document.getElementById("tbNgay").innerHTML = `Vui lòng không để trống`;
        return false;
    }
    else {
        document.getElementById("tbNgay").style.display = "block";
        document.getElementById("tbNgay").innerHTML = "Vui lòng nhập theo mẫu: nn/tt/nnnn";
        return false;
    }
}

function kiemTraMatKhau(value) {
    var p_reg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/;

    if (value.match(p_reg)) {
        document.getElementById("tbMatKhau").style.display = "none";
        return true;
    } else if (value == "") {
        document.getElementById("tbMatKhau").style.display = "block";
        document.getElementById("tbMatKhau").innerHTML = "Vui lòng nhập mật khẩu!";
        return false;
    } else if (value.length < 6) {
        document.getElementById("tbMatKhau").style.display = "block";
        document.getElementById("tbMatKhau").innerHTML = "Mất khẩu phải nhiều hơn 6 ký tự!";
        return false;
    } else {
        document.getElementById("tbMatKhau").style.display = "block";
        document.getElementById("tbMatKhau").innerHTML = "Mật khẩu phải có ít nhất 1 ký tự số, ít nhất 1 ký tự chữ in hoa và ít nhất 1 ký tự chữ thường";
        return false;
    }
}

var kiemTraLuong = (valueCheck) => {
    var reg = /^\d+$/;
    var min = 1000000, max = 20000000;
    if (valueCheck < min || valueCheck > max) {
        document.getElementById('tbLuongCB').style.display = "block";
        document.getElementById('tbLuongCB').innerHTML =
            `LLương cơ bản phải từ 1000000 ~ 20000000. Vui lòng nhập lại`;
        return false;
    } else if (reg.test(valueCheck)) {
        document.getElementById('tbLuongCB').style.display = "none";
        return true;
    } else if (valueCheck == "") {
        document.getElementById('tbLuongCB').style.display = "block";
        document.getElementById('tbLuongCB').innerHTML = `Vui lòng nhập lương!`;
        return false;
    }

}

var kiemTraGioLam = (minTime, maxTime, valueCheck) => {
    var gioLam = document.getElementById('tbGiolam');
    if (valueCheck == "") {
        gioLam.style.display = "block";
        gioLam.innerHTML = `Vui lòng nhập giờ làm!`;
        return false;
    } else if (valueCheck < minTime || valueCheck > maxTime) {
        gioLam.style.display = "block";
        gioLam.innerHTML = `Giờ làm phải từ ${minTime} đến ${maxTime}. Vui lòng nhập lại!`;
        return false;
    } else {
        gioLam.style.display = "none";
        return true;
    }
}

var kiemTraChucVu = (valueCheck) => {

    if (valueCheck == 0) {
        document.getElementById('tbChucVu').style.display = "block";
        document.getElementById('tbChucVu').innerHTML = `Vui long chon chuc vu`;
        return false;
    } else {
        document.getElementById('tbChucVu').style.display = "none";
        return true;
    }
}
